var request = require('request');

describe('Service route', function() {
    var server;
    
    beforeAll(() => {
        server = require("../../src/app");
    });
    
    afterAll(() => {
        server.close();
    });
    
    describe('GET /', function() {
        it('returns true if the service can properly respond to calls', function(done) {
            request({
                headers: {
                    'Content-Type': 'application/json'
                },
                uri: 'http://localhost:3000',
                method: 'GET'
            }, function(error, response, body) {
                var data = JSON.parse(body);

                expect(response.statusCode).toBe(200);
                expect(data.can_connect_to_db).toBe(true);
                expect(data.can_connect_to_auth).toBe(true);
                expect(data.can_connect_to_streaming).toBe(true);

                done();
            });
        });
    });

    describe('GET /count/:user_id', function() {
        it('returns 422 because it can use a parameter', function(done) {
            request({
                headers: {
                    'Content-Type': 'application/json'
                },
                uri: 'http://localhost:3000/count/badparam',
                method: 'GET'
            }, function(error, response, body) {
                expect(response.statusCode).toBe(422);

                done();
            });
        });

        it('returns 404 because it cannot find a user', function(done) {
            request({
                headers: {
                    'Content-Type': 'application/json'
                },
                uri: 'http://localhost:3000/count/99',
                method: 'GET'
            }, function(error, response, body) {
                expect(response.statusCode).toBe(404);

                done();
            });
        });

        it('returns a user with ID=1 who cannot add a stream', function(done) {
            request({
                headers: {
                    'Content-Type': 'application/json'
                },
                uri: 'http://localhost:3000/count/1',
                method: 'GET'
            }, function(error, response, body) {                
                var data = JSON.parse(body);
                
                expect(response.statusCode).toBe(200);
                expect(data.user_id).toBe(1);
                expect(data.active_stream_count).toBe(3);
                expect(data.can_add_stream).toBe(false);

                done();
            });
        });

        it('returns a user with ID=2 who can add a stream', function(done) {
            request({
                headers: {
                    'Content-Type': 'application/json'
                },
                uri: 'http://localhost:3000/count/2',
                method: 'GET'
            }, function(error, response, body) {                
                var data = JSON.parse(body);
                
                expect(response.statusCode).toBe(200);
                expect(data.user_id).toBe(2);
                expect(data.active_stream_count).toBe(1);
                expect(data.can_add_stream).toBe(true);

                done();
            });
        });
    });
});