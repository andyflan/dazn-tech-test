var user_model = require('../../src/models/user');

describe('User model', function() {    
    it('returns a list of user data', function(done) {
        var user_data_list = user_model.get_users();

        expect(user_data_list.length).toBe(6); 

        done();
    });

    it('returns a user with id=2', function(done) {
        var user_data = user_model.get_user(2);

        expect(user_data.id).toBe(2); 
        expect(user_data.active_stream_count).toBe(1); 

        done();
    });
});