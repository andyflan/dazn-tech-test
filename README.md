# DAZN Tech Test - Andrew Flannery

## Usage

### Installing for development

1. Clone the repo into the current dir: `git clone git@gitlab.com:andyflan/dazn-tech-test.git .`.
2. Install the dependencies: `npm install`.
3. Run the dev server: `npm start`

### Run the tests

1. Install the dependencies: `npm install`.
2. Run the tests: `npm test`.

### Access the API

#### `GET /`

*Parameters:* None.

*Result:*

Status = 200
```
{
    "can_connect_to_db": true,
    "can_connect_to_auth": true,
    "can_connect_to_streaming": true
}

```

#### `GET /count/:user_id`

*Parameters:*

`user_id` _int_ The id of the user

*Results:*

Status = 200

```
{
    "user_id": 1,
    "active_stream_count": 3,
    "can_add_stream": false
}

```

Status = 404

_A user with the given :user_id is not found_


Status = 422

_The `:user_id` cannot be parsed as an integer_

## Commentary

### Choosing the tech test

I decided to do the node.js test because node has a built in dev server, so it's easy for another developer to get the code up and 
running. Other platforms, such as Django and Ruby on Rails have a built in dev server but I decided against using these because 
DAZN projects mostly run on node so the dependencies of this project are more likely to be easily met by a DAZN dev.

### Concepts arising from the brief and their definitions

*Client:* A client can be any HTTP client, such as a web browser, another script, a testing environment such as Postman or an app, 
etc.

*User:* A account registered in the system somewhere which is refered to by an ID.

*Video stream:* A user can watch videos. Each time a user starts a new stream in a new client or client session, their stream count 
is incremented or decremented when the stream is ended or times out.