const express = require('express')
const app = express()
const port = 3000

var service_routes = require('./routes/service');

app.use('/', service_routes);

var server = app.listen(port, () => console.log(`DAZN test listening on port ${port}!`))

module.exports = server;