var express = require('express');
var router = express.Router();
var response_codes = require('../shared/response_codes');
var has_param = require('../shared/has_param');
var service_controller = require('../controllers/service');

/* GET root address - Service status */
router.get('/', function(req, res, next) {
    var service_status_data = service_controller.service_status();

    // TODO: Support nore nuanced HTTP status codes

    res.status(response_codes.SUCCESS).json(service_status_data);
});

/* GET the count for the given user */
router.get('/count/:user_id', function(req, res, next) {
    var user_id = has_param(req.params, 'user_id') ? req.params.user_id : false;

    if (user_id === false || isNaN(parseInt(user_id))) {
        res.status(response_codes.MISSING_PARAM).send();

        return;
    }

    var check_user_stream_count_data = service_controller.check_user_stream_count(user_id);

    if (check_user_stream_count_data == false) {
        res.status(response_codes.NO_RECORD_FOUND).send();;

        return;
    }

    res.status(response_codes.SUCCESS).json(check_user_stream_count_data);
});

module.exports = router;