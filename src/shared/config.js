var path = require("path");

var env = process.env.NODE_ENV || "development";
var config = require(path.join(__dirname, '..', 'config.json'))[env];

module.exports = config;