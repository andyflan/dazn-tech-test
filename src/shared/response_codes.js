module.exports = {
    SUCCESS: 200,
    MISSING_PARAM: 422,
    NO_RECORD_FOUND: 404
};