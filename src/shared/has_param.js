module.exports = function(object, param_name) {
    return typeof object[param_name] !== 'undefined' && object[param_name] !== null && String(object[param_name]).trim() !== '';
}