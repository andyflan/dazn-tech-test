var user_data = require("../data/users.json");

var model = {
    get_users: function() {
        return user_data;
    },

    get_user: function(user_id) {
        var users = this.get_users();
        var result_user = null;

        users.forEach(function(user) {
            if (user.id === user_id) {
                result_user = user;
            }
        });

        return result_user;
    }
};

module.exports = model;