var config = require("../shared/config.js");
var user_model = require("../models/user.js");

var controller = {
    service_status: function() {
        var is_available = true;
        
        // TODO: Check that any dependent services, such as DB, are available 
        
        return {
            can_connect_to_db: is_available,
            can_connect_to_auth: is_available,
            can_connect_to_streaming: is_available
        };
    },

    check_user_stream_count: function(user_id) {
        var user = user_model.get_user(parseInt(user_id));

        if (user == null) {
            return false;
        }

        return {
            user_id: user.id,
            active_stream_count: user.active_stream_count,
            can_add_stream: user.active_stream_count < config.stream_limit
        }
    }
};

module.exports = controller;